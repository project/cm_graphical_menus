<?php
/**
 * @file
 * cm_graphical_menus.features.inc
 */

/**
 * Implements hook_views_api().
 */
function cm_graphical_menus_views_api() {
  return array("version" => "3.0");
}
