<?php
/**
 * @file
 * cm_graphical_menus.features.taxonomy.inc
 */

/**
 * Implements hook_taxonomy_default_vocabularies().
 */
function cm_graphical_menus_taxonomy_default_vocabularies() {
  return array(
    'icon_menu' => array(
      'name' => 'Icon menu',
      'machine_name' => 'icon_menu',
      'description' => 'The menu with icons under the header',
      'hierarchy' => '0',
      'module' => 'taxonomy',
      'weight' => '0',
      'rdf_mapping' => array(
        'rdftype' => array(
          0 => 'skos:ConceptScheme',
        ),
        'name' => array(
          'predicates' => array(
            0 => 'dc:title',
          ),
        ),
        'description' => array(
          'predicates' => array(
            0 => 'rdfs:comment',
          ),
        ),
      ),
    ),
  );
}
