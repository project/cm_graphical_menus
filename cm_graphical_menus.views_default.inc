<?php
/**
 * @file
 * cm_graphical_menus.views_default.inc
 */

/**
 * Implements hook_views_default_views().
 */
function cm_graphical_menus_views_default_views() {
  $export = array();

  $view = new view();
  $view->name = 'icons_menu';
  $view->description = '';
  $view->tag = 'default';
  $view->base_table = 'taxonomy_term_data';
  $view->human_name = 'Icons menu';
  $view->core = 7;
  $view->api_version = '3.0';
  $view->disabled = FALSE; /* Edit this to true to make a default view disabled initially */

  /* Display: Master */
  $handler = $view->new_display('default', 'Master', 'default');
  $handler->display->display_options['title'] = 'Icons menu';
  $handler->display->display_options['use_more_always'] = FALSE;
  $handler->display->display_options['access']['type'] = 'perm';
  $handler->display->display_options['cache']['type'] = 'none';
  $handler->display->display_options['query']['type'] = 'views_query';
  $handler->display->display_options['exposed_form']['type'] = 'basic';
  $handler->display->display_options['pager']['type'] = 'some';
  $handler->display->display_options['pager']['options']['items_per_page'] = '5';
  $handler->display->display_options['style_plugin'] = 'list';
  $handler->display->display_options['style_options']['wrapper_class'] = 'icons-menu';
  $handler->display->display_options['row_plugin'] = 'fields';
  /* Field: Taxonomy term: Icon */
  $handler->display->display_options['fields']['field_iconmenu_icon']['id'] = 'field_iconmenu_icon';
  $handler->display->display_options['fields']['field_iconmenu_icon']['table'] = 'field_data_field_iconmenu_icon';
  $handler->display->display_options['fields']['field_iconmenu_icon']['field'] = 'field_iconmenu_icon';
  $handler->display->display_options['fields']['field_iconmenu_icon']['label'] = '';
  $handler->display->display_options['fields']['field_iconmenu_icon']['exclude'] = TRUE;
  $handler->display->display_options['fields']['field_iconmenu_icon']['element_type'] = '0';
  $handler->display->display_options['fields']['field_iconmenu_icon']['element_label_colon'] = FALSE;
  $handler->display->display_options['fields']['field_iconmenu_icon']['element_wrapper_type'] = '0';
  $handler->display->display_options['fields']['field_iconmenu_icon']['element_default_classes'] = FALSE;
  $handler->display->display_options['fields']['field_iconmenu_icon']['click_sort_column'] = 'fid';
  $handler->display->display_options['fields']['field_iconmenu_icon']['settings'] = array(
    'image_style' => '',
    'image_link' => '',
  );
  /* Field: Taxonomy term: Name */
  $handler->display->display_options['fields']['name']['id'] = 'name';
  $handler->display->display_options['fields']['name']['table'] = 'taxonomy_term_data';
  $handler->display->display_options['fields']['name']['field'] = 'name';
  $handler->display->display_options['fields']['name']['label'] = '';
  $handler->display->display_options['fields']['name']['exclude'] = TRUE;
  $handler->display->display_options['fields']['name']['alter']['word_boundary'] = FALSE;
  $handler->display->display_options['fields']['name']['alter']['ellipsis'] = FALSE;
  $handler->display->display_options['fields']['name']['element_label_colon'] = FALSE;
  /* Field: Taxonomy term: Link */
  $handler->display->display_options['fields']['field_link']['id'] = 'field_link';
  $handler->display->display_options['fields']['field_link']['table'] = 'field_data_field_link';
  $handler->display->display_options['fields']['field_link']['field'] = 'field_link';
  $handler->display->display_options['fields']['field_link']['label'] = '';
  $handler->display->display_options['fields']['field_link']['exclude'] = TRUE;
  $handler->display->display_options['fields']['field_link']['element_type'] = '0';
  $handler->display->display_options['fields']['field_link']['element_label_colon'] = FALSE;
  $handler->display->display_options['fields']['field_link']['element_wrapper_type'] = '0';
  $handler->display->display_options['fields']['field_link']['element_default_classes'] = FALSE;
  /* Field: Global: Custom text */
  $handler->display->display_options['fields']['nothing']['id'] = 'nothing';
  $handler->display->display_options['fields']['nothing']['table'] = 'views';
  $handler->display->display_options['fields']['nothing']['field'] = 'nothing';
  $handler->display->display_options['fields']['nothing']['label'] = '';
  $handler->display->display_options['fields']['nothing']['alter']['text'] = '<a href="[field_link]" class="inner">
  <div class="li-img">[field_iconmenu_icon] </div>
  <div class="li-text">[name]</div>
</a>';
  $handler->display->display_options['fields']['nothing']['element_label_colon'] = FALSE;
  /* Filter criterion: Taxonomy vocabulary: Machine name */
  $handler->display->display_options['filters']['machine_name']['id'] = 'machine_name';
  $handler->display->display_options['filters']['machine_name']['table'] = 'taxonomy_vocabulary';
  $handler->display->display_options['filters']['machine_name']['field'] = 'machine_name';
  $handler->display->display_options['filters']['machine_name']['value'] = array(
    'icon_menu' => 'icon_menu',
  );

  /* Display: Block */
  $handler = $view->new_display('block', 'Block', 'block');
  $handler->display->display_options['defaults']['hide_admin_links'] = FALSE;
  $handler->display->display_options['block_description'] = 'Icons menu';
  $translatables['icons_menu'] = array(
    t('Master'),
    t('Icons menu'),
    t('more'),
    t('Apply'),
    t('Reset'),
    t('Sort by'),
    t('Asc'),
    t('Desc'),
    t('<a href="[field_link]" class="inner">
  <div class="li-img">[field_iconmenu_icon] </div>
  <div class="li-text">[name]</div>
</a>'),
    t('Block'),
  );
  $export['icons_menu'] = $view;

  return $export;
}
